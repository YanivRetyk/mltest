####Q1
#a
setwd("C:/Users/retikya/Desktop/tesyml")
school.raw <- read.csv("���� ����� - ������ ���� ����� ������ 10441.csv")

school <- school.raw
str(school)
summary(school)


#b
#checking for NA's
 any(is.na(school))

#c
 #cheking conection betwin higher and G3
 library(ggplot2)
 ggplot(school, aes(higher ,G3)) + geom_boxplot()

 ###Q2
 #a
 ggplot(school, aes(school ,G3)) + geom_boxplot()

 #b
 ggplot(school, aes(x=absences)) + geom_histogram()
school$absences
#fix outstandig absences
clean <- function(x,beater){
  if (x>beater) return(beater)
  return(x)
}
school$absences <- sapply(school$absences,clean, beater=31)
#c
#building a new boolian column did sexcid in school or not
school$success <- school$G3>10
ggplot(school, aes(success ,absences)) + geom_boxplot()

#d
ggplot(school, aes(success ,absences)) + geom_boxplot()
ggplot(school, aes(failures, fill = success)) + geom_bar(position='fill')

####Q3
#a
library(caTools)

filter <- sample.split(school$G3 , SplitRatio = 0.7)

school.train <- subset(school, filter ==T)

school.test <- subset(school, filter ==F)


dim(school.train)
dim(school.test)

#b
#liniar regretiom model -> without column success. because it curaps model
modellr <- lm(G3 ~ .-success, school.train)

summary(modellr)

predicted.train <- predict(modellr,school.train)
predicted.test <- predict(modellr,school.test)


MSE.train <- mean((school.train$G3 - predicted.train)**2)# MSE.train => 8.144798
MSE.test <- mean((school.test$G3 - predicted.test)**2) # MSE.test => 8.259016

AIC(modellr)  # AIC => 1419.066
BIC(modellr)  # BIC => 1513.291

#c
real.lr <- school.test$G3 >=11
actual.lr <- predicted.test>=11

#confusion matrix 
conf_matrix_lr <- table(actual.lr,real.lr)

length(predicted.lr)
length(actual.lr)


####4
#a
#excelent is gread 15 and higher
summary(school)
dim(school)
school$excelent <- school$G3>=15

#b-
filtern <- sample.split(school$excelent , SplitRatio = 0.7)

schoolnew.train <- subset(school, filtern ==T)

schoolnew.test <- subset(school, filtern ==F)

dim(schoolnew.train)
dim(schoolnew.test)

#run naive base algorithm to generate a model 
#install.packages('e1071')
library(e1071)

modelbn <- naiveBayes(schoolnew.train, schoolnew.train$excelent)

#prediction on the test set 
prediction <- predict(modelbn, schoolnew.test, type = 'raw')

prediction_bn <- prediction[,'TRUE']
actual <- schoolnew.test$excelent
predicted <- prediction_bn > 0.5

#confusion matrix 
conf_matrix <- table(predicted,actual)

summary(modelbn)
#compute precision and recall 
precision <- conf_matrix[2,2]/(conf_matrix[2,2]+conf_matrix[2,1])
recalol <- conf_matrix[2,2]/(conf_matrix[2,2]+conf_matrix[1,2])
precision
recalol

#c-
#install.packages('rpart')
library(rpart)
#install.packages('rpart.plot')
library(rpart.plot)

#b
model.rp <- rpart(excelent~.,schoolnew.train)
rpart.plot(model.rp, box.palette="RdBu", shadow.col="gray", nn=TRUE)

predict.prob <- predict(model.rp,schoolnew.test)
predict.prob.exce <- predict.prob


#d
#ROC curve 
#install.packages('pROC')
library(pROC)

rocCurveTR <- roc(schoolnew.test$excelent, prediction_bn, direction = ">", levels = c("TRUE","FALSE"))
rocCurveNB <- roc(schoolnew.test$excelent, predict.prob.exce, direction = ">", levels = c("TRUE","FALSE"))


#Calculate AUC
auc(rocCurveTR)
auc(rocCurveNB)
#Naive base is a little better


plot(rocCurveTR, col="red", main='ROC chart')
par(new=TRUE)
plot(rocCurveNB, col="blue", main='ROC chart')























